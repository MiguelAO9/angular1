import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Feedback, ContactType} from '../shared/feedback'
import { FeedbackService } from '../services/feedback.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { flyInOut, expand } from '../animations/app.animations'

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'stlye' : 'display: block',

  },
  animations: [
    flyInOut(),
    expand(),
  ]
})

export class AppointmentComponent implements OnInit {

  name: string;
  email:string;
  date: string;
  problem: string;
  @ViewChild('fform') feedbackFormDirective;
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }



  onSubmit(){
    this.name = ''
    this.email = ''
    this.date = ''
    this.problem = ''
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
