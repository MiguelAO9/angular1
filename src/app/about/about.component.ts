import { Component, OnInit, Inject } from '@angular/core';

import { flyInOut, expand } from '../animations/app.animations'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'stlye' : 'display: block',

  },
  animations: [
    flyInOut(),
  ]
})
export class AboutComponent implements OnInit {

  constructor(
    @Inject('BaseURL' ) private BaseURL) { }


  errMsg :string;

  ngOnInit(): void {


  }

}
