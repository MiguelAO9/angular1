import { Component, OnInit } from '@angular/core';
import { services } from '../shared/services';
import { Service } from '../shared/service';

@Component({
  selector: 'app-services-work',
  templateUrl: './services-work.component.html',
  styleUrls: ['./services-work.component.scss']
})
export class ServicesWorkComponent implements OnInit {

  services: Service[];

  constructor() { }

  ngOnInit(): void {
    this.services = services;
  }

}
