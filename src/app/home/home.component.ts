import { Component, OnInit, Inject } from '@angular/core';


import { flyInOut, expand } from '../animations/app.animations'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'stlye' : 'display: block',

  },
  animations: [
    flyInOut(),
    expand(),
  ]
})

export class HomeComponent implements OnInit {


  dishErrMsg: string;
  leaderErrMsg: string;
  promoErrMsg: string;


  constructor(
    @Inject('BaseURL' ) private BaseURL) { }

  ngOnInit() {

  }


}

