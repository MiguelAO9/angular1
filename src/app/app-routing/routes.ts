import {Routes} from '@angular/router';

import  { AppointmentComponent } from '../appointment/appointment.component'
import  { ServicesWorkComponent } from '../services-work/services-work.component'
import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component'
import { AppComponent } from '../app.component';


export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'contactus',     component: ContactComponent },
  {path: 'about',     component: AboutComponent },
  {path: 'services',     component: ServicesWorkComponent },
  {path: 'appointment',     component: AppointmentComponent },
  {path: '', redirectTo: '/home', pathMatch: 'full'},
]
