export class Appointment {
  name: string;
  email: string;
  description: string;
  date: string;
}
