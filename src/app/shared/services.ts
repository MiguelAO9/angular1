import { Service } from './service'

export const services: Service[] = [

  {
    name: "Oil change" ,
    description: "Engine oil is the lifeblood of the engine. The oil resides in the oil pan, which is under the car attached to the bottom of the engine. All internal (moving) parts of the engine need to be lubricated by the engine oil. Inadequate lubrication will cause the parts to wear out faster and eventually lead to engine failure. An oil filter keeps the oil clean and free of debris. If the filter is not replaced on a regular basis, it will get clogged and will not be able to pass oil into the engine." ,
    image:  "../../assets/images/oilchange.jpg",
    priceStimated: "$92 - $157"
  },
  {
    name: "Check engine light" ,
    description: "The Check Engine Light can be one of the most confusing warnings in your dashboard cluster because it offers no explanation as to why you suddenly need to check your engine. It sounds ominous and can be as serious as a malfunctioning catalytic converter or as trivial as a loose gas cap." ,
    image:  "../../assets/images/enginelight.jpg",
    priceStimated: "$80 - $90"
  },
  {
    name: "Car is not starting Inspection" ,
    description: "A car that fails to start is a rare occurrence in this age of greatly improved efficiency and reliability. But it still happens occasionally, and the advanced electronic technologies that have made cars better have also made the job of roadside diagnosis difficult, if not impossible, for even reasonably handy owners. One of our expert mobile mechanics can provide an expeditious diagnosis and provide a quote for any necessary repairs." ,
    image:  "../../assets/images/carinspection.jpg",
    priceStimated: "$80 - $90"
  },
  {
    name: "Battery replacement" ,
    description: "A car battery is an energy storage device that relies on a chemical reaction within the battery to produce electricity. The stored electrical energy is used to initially operate the starter motor, ignition system, and fuel system on your vehicle. Once your car engine is running, the alternator supplies the electricity needed for all vehicle systems and charges the battery to replace the electric energy used when starting a car." ,
    image:  "../../assets/images/batteryreplacement.jpg",
    priceStimated: "$200 - $320"
  },
  {
    name: "Transmission Fluid Service" ,
    description: "Transmission fluid helps cool and lubricate your vehicle’s transmission’s internal parts. The transmission transfers power from the engine to the wheels of the car. An automatic transmission shifts the car into different gears depending on the speed that you are driving. In an automatic transmission car, the transmission system has an oil pan that contains the transmission fluid and fluid filter. The fluid provides the power/force required to shift into different gears. In manual transmission cars, there is a case that contains the transmission fluid." ,
    image:  "../../assets/images/transmissionfluid.jpeg",
    priceStimated: "$125 - $440"
  },
  {
    name: "AC accumulator replacement" ,
    description: "It takes a lot of technology to make the inside of your car comfortable during the heat of summer. And while you might be familiar with most of the major A/C components, like the compressor, chances are good that you may not be familiar with the accumulator, which is a lesser known part." ,
    image:  "../../assets/images/acaccumulator.png",
    priceStimated: "$105 - $703"
  },

  ]
