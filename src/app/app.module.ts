import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule} from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { CommonModule} from '@angular/common';
import { MatCardModule } from  '@angular/material/card';
import { MatButtonModule } from  '@angular/material/button';
import { MatDialogModule} from '@angular/material/dialog'
import { MatFormFieldModule} from '@angular/material/form-field'
import { MatInputModule} from '@angular/material/input'
import { MatCheckboxModule} from '@angular/material/checkbox'
import { MatSelectModule} from '@angular/material/select'
import { MatSlideToggleModule} from '@angular/material/slide-toggle'
import { FormsModule} from '@angular/forms'
import { ReactiveFormsModule} from '@angular/forms'
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner'
import {MatSliderModule} from '@angular/material/slider'
import { HttpClientModule} from '@angular/common/http';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatIconModule} from '@angular/material/icon';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { ProcessHTTPMsgService} from './services/process-httpmsg.service'

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component'
import  { AppointmentComponent } from './appointment/appointment.component'
import  { ServicesWorkComponent } from './services-work/services-work.component'

import { AppComponent } from './app.component';
import 'hammerjs';
import { AppRoutingModule} from './app-routing/app-routing.module'

import { baseURL} from './shared/baseurl';
import { HighlightDirective } from './directives/highlight.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    HighlightDirective,
    AppointmentComponent,
    ServicesWorkComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatListModule,
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    AppRoutingModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    HttpClientModule,
    MatDatepickerModule,
    MatIconModule,
    MatNativeDateModule,
    MatRippleModule,
    MatSnackBarModule,

  ],
  entryComponents: [

  ],
  providers: [
    ProcessHTTPMsgService,
    { provide: 'BaseURL', useValue: baseURL},

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
